
Raven.config('https://5e66865d66a34202bf674a41346ab1b6@sentry.io/285515').install();

/**
 * Report a routing error to Sentry and show a feedback dialog to
 * the user.
 * 
 * > try {
 * >   renderRoute()
 * > } catch (err) {
 * >   handleRouteError(err);
 * > }
 */
function handleRouteError(err) {
    Raven.captureException(err);
    Raven.showReportDialog();
};

handleRouteError(a);